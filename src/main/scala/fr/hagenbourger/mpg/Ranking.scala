package fr.hagenbourger.mpg

import net.liftweb.json.JsonAST.JValue
import net.liftweb.json.{DefaultFormats, JsonAST}
import org.apache.commons.text.StringEscapeUtils

case class Ranking(
                    teamid: String,
                    rotaldo: Boolean,
                    targetMan: Option[Boolean],
                    bonusUser: Boolean,
                    series: String,
                    played: Int,
                    victory: Int,
                    draw: Int,
                    defeat: Int,
                    goal: Int,
                    goalconceded: Int,
                    difference: Int,
                    points: Int,
                    rank: Int,
                    variation: Int
                  ) {
  implicit val formats: DefaultFormats = DefaultFormats

  def goalEquity(totalGoal: Int, numberOfTeams: Int): Float = (totalGoal - goal) / (numberOfTeams - 1).toFloat

  def luck(totalGoal: Int, numberOfTeams: Int): Float = luck(goalEquity(totalGoal, numberOfTeams))

  def name(teams: JValue): String = (teams \ teamid \ "name").extract[String]

  def htmlName(teams: JValue): String = StringEscapeUtils.escapeHtml4(name(teams))

  def toHtmlTable(teams: JsonAST.JValue, totalGoals: Int, numberOfTeams: Int): String = {
    s"<tr>" +
      s"<td>${htmlName(teams)}</td>" +
      s"<td>${this.rank}</td>" +
      s"<td>${this.played}</td>" +
      s"<td>${this.victory}</td>" +
      s"<td>${this.draw}</td>" +
      s"<td>${this.defeat}</td>" +
      s"<td>${this.goal}</td>" +
      s"<td>${this.goalconceded}</td>" +
      s"<td>${this.points}</td>" +
      f"<td>${goalEquity(totalGoals, numberOfTeams)}%1.1f</td>" +
      f"<td>${luck(totalGoals, numberOfTeams) * 100}%1.0f" + " %</td>" +
      s"</tr>"
  }

  private def luck(equity: Float): Float = if (equity > goalconceded) equity / goalconceded - 1 else -goalconceded / equity + 1
}
