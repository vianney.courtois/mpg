name := "mpg"
version := "0.1"
scalaVersion := "2.13.3"

resolvers += "mvnrepository" at "https://mvnrepository.com/artifact/"
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.6"
libraryDependencies += "net.liftweb" %% "lift-json" % "3.4.2"
libraryDependencies += "org.apache.commons" % "commons-text" % "1.9"
